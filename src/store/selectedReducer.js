const localStorageDataSelected = JSON.parse(localStorage.getItem("counterSelected"));

const initialState = {
    counterSelected: localStorageDataSelected || 0
};

export const UPDATE_SELECTED = 'UPDATE_SELECTED'

const selectedReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_SELECTED:
            return {
                ...state,
                counterSelected: action.payload,
            };
        default:
            return state;
    }
};

export default selectedReducer;