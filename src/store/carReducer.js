const localStorageDataCars = JSON.parse(localStorage.getItem("racingCars"));

const initialState = {
    cars: localStorageDataCars || [],
};

export const UPDATE_CAR = "UPDATE_CAR";

const carReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_CAR:
            return {
                ...state,
                cars: action.payload,
            };
        default:
            return state;
    }
};

const fetchUsersAC = (cars) => ({type: UPDATE_CAR, payload: cars});

export const fetchUsersThunk = () => {
    return (dispatch) => {
        fetch('/products.json')
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Error executing the request');
                }
                return response.json();
            })
            .then((data) => {
                data.racingCars.forEach((data, index) => {
                    data.completed = false;
                    data.idCar = index;
                    data.buyCar = false;
                    index++;
                });
                dispatch(fetchUsersAC(data.racingCars));
            })
            .catch((error) => {
                console.error('Error executing the request:', error);
            });
    }
};

export default carReducer;