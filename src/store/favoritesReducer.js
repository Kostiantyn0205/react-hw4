const localStorageDataFavorites = JSON.parse(localStorage.getItem("counterFavorites"));

const initialState = {
    counterFavorites: localStorageDataFavorites || 0,
};

export const UPDATE_FAVORITES = 'UPDATE_FAVORITES'

const favoritesReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_FAVORITES:
            return {
                ...state,
                counterFavorites: action.payload,
            };
        default:
            return state;
    }
};

export default favoritesReducer;