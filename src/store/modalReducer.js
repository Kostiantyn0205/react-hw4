const initialState = {
    showModal: false,
    modals: {},
};

export const TOGGLE_MODAL = 'TOGGLE_MODAL';

export const toggle = (cardId, modalState) => ({
    type: "TOGGLE_MODAL",
    cardId,
    modalState,
});

const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_MODAL:
            return {
                ...state,
                modals: {
                    ...state.modals,
                    [action.cardId]: action.modalState,
                },
            };
        default:
            return state;
    }
};

export default modalReducer;