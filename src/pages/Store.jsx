import ListCard from "../components/ListCard;";
import PropTypes from "prop-types";

function Store({toggleCounterSelected, toggleCounterFavorites}) {
    return (
        <ListCard toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites}></ListCard>
    )
}

Store.propTypes = {
    racingCars: PropTypes.array,
    toggleCounterSelected: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default Store;