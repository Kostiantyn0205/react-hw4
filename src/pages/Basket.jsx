import ListCard from "../components/ListCard;";
import PropTypes from "prop-types";

function Basket({toggleCounterSelected, toggleCounterFavorites}){
    return (
        <ListCard pageName="Basket" toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites}></ListCard>
    )
}

Basket.propTypes = {
    toggleCounterSelected: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default Basket;