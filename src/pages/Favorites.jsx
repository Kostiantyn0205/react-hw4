import ListCard from "../components/ListCard;";
import PropTypes from "prop-types";

function Favorites({toggleCounterSelected, toggleCounterFavorites}){
    return (
        <ListCard pageName="Favorites" toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites}></ListCard>
    )
}

Favorites.propTypes = {
    toggleCounterSelected: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default Favorites;