import './scss/App.scss';
import {useEffect} from 'react';
import {Routes, Route} from "react-router-dom";
import Store from "./pages/Store"
import Basket from "./pages/Basket"
import Favorites from "./pages/Favorites"
import Header from "./components/Header";
import {useDispatch, useSelector} from "react-redux";
import {fetchUsersThunk, UPDATE_CAR} from "./store/carReducer";
import {UPDATE_FAVORITES} from "./store/favoritesReducer";
import {UPDATE_SELECTED} from "./store/selectedReducer";

function App() {
    const dispatch = useDispatch();
    const localStorageDataCars = JSON.parse(localStorage.getItem("racingCars"));

    const cars = useSelector((state) => state.car.cars);
    const counterFavorites = useSelector((state) => state.favorites.counterFavorites);
    const counterSelected = useSelector((state) => state.choose.counterSelected);

    useEffect(() => {
        if(!localStorageDataCars) {
            dispatch(fetchUsersThunk());
        }
    }, []);

    useEffect(() => {
        localStorage.setItem("counterFavorites", JSON.stringify(counterFavorites));
        localStorage.setItem("counterSelected", JSON.stringify(counterSelected));
    }, [counterSelected, counterFavorites]);

    const toggleCounterFavorites = (id) => {
        console.log(id)
        const newRacingCars = [...cars];
        newRacingCars[id].completed = !newRacingCars[id].completed;
        newRacingCars[id].completed ? dispatch({type: UPDATE_FAVORITES, payload: counterFavorites + 1}) : dispatch({type: UPDATE_FAVORITES, payload: counterFavorites - 1});

        localStorage.setItem("racingCars", JSON.stringify(newRacingCars));
        dispatch({ type: UPDATE_CAR, payload: newRacingCars });
    }

    const toggleCounterSelected = (id) => {
        console.log(id)
        const newRacingCars = [...cars];
        newRacingCars[id].buyCar = !newRacingCars[id].buyCar;
        newRacingCars[id].buyCar ? dispatch({type: UPDATE_SELECTED, payload: counterSelected + 1}) : dispatch({type: UPDATE_SELECTED, payload: counterSelected - 1});

        localStorage.setItem("racingCars", JSON.stringify(newRacingCars));
        dispatch({ type: UPDATE_CAR, payload: newRacingCars });
    }

    return (
        <>
            <Header />
            <Routes>
                <Route index path="/" element={<Store toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites}/>}/>
                <Route path="/favorites" element={<Favorites toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites}/>}/>}/>}/>
                <Route path="/basket" element={<Basket toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites}/>}/>}/>
            </Routes>
        </>
    );
}

export default App;